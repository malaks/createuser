package model;

import java.sql.CallableStatement;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import model.VO.YpouUsersVOImpl;

import model.common.createUserAppModule;

import oracle.jbo.JboException;
import oracle.jbo.server.ApplicationModuleImpl;

import oracle.jbo.server.ViewObjectImpl;

import weblogic.management.utils.AlreadyExistsException;
import weblogic.management.utils.InvalidParameterException;
import weblogic.management.utils.NotFoundException;

public class createUserAppModuleImpl extends ApplicationModuleImpl implements createUserAppModule {
    public createUserAppModuleImpl() {
    }

    protected void callStoredProcedure(String stmt, Object[] bindVars) {
        CallableStatement st = null;
        try {
            st = getDBTransaction().createCallableStatement("begin " + stmt + ";end;", 0);
            if (bindVars != null) {
                for (int z = 0; z < bindVars.length; z++) {
                    st.setObject(z + 1, bindVars[z]);
                }
            }
            st.executeUpdate();
        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                }
            }
        }
    }    
    
    public void setNewUserToDB(String userId,String userDescription, String eMail, String firstName, String lastName, String parentUserId) {
        callStoredProcedure("set_new_user(?,?,?,?,?,?)", new Object[] { userId, userDescription, eMail, firstName, lastName, parentUserId});
    }    
    
    public String createUser(String userId,String userPassword,String userDescription, String eMail, String firstName, String lastName, String parentUserId, ArrayList groupNames) {
        WlsUtils wl = new WlsUtils();
        String result = wl.createUser(userId, userPassword, userDescription, eMail, groupNames);
        /*if (result == "success")
        {
            setNewUserToDB(userId, userDescription, eMail, firstName, lastName, parentUserId);
            return "success";
        }*/
        return result;
    }
    
    public String removeSelectedUser(String username){
        WlsUtils wl = new WlsUtils();
        String result = wl.removeUser(username);
        return result;
    }
    
    public String removeGroup(String groupName){
        WlsUtils wl = new WlsUtils();
        String result = wl.removeGroup(groupName);
        return result;
    }
    
    public List<String> getAllUsers(){
        WlsUtils wl = new WlsUtils();
        return wl.getListOfUsers();
    }
    
    public  List getGroups(String groupName) {
        WlsUtils wl = new WlsUtils();
        return wl.listGroups(groupName);
    }

    /**
     * Container's getter for YmsuCustomersGrupVVO1.
     * @return YmsuCustomersGrupVVO1
     */
    public ViewObjectImpl getYMSUCustomersGrupVVOBayi() {
        return (ViewObjectImpl)findViewObject("YMSUCustomersGrupVVOBayi");
    }

    /**
     * Container's getter for YmsuIllerVO1.
     * @return YmsuIllerVO1
     */
    public ViewObjectImpl getYmsuIllerVO1() {
        return (ViewObjectImpl)findViewObject("YmsuIllerVO1");
    }

    /**
     * Container's getter for YmsuTabletSatisTemsilcisiVO1.
     * @return YmsuTabletSatisTemsilcisiVO1
     */
    public ViewObjectImpl getYmsuTabletSatisTemsilcisiVO1() {
        return (ViewObjectImpl)findViewObject("YmsuTabletSatisTemsilcisiVO1");
    }

    /**
     * Container's getter for YpouUsersVO1.
     * @return YpouUsersVO1
     */
    public ViewObjectImpl getYpouUsersVO1() {
        return (ViewObjectImpl)findViewObject("YpouUsersVO1");
    }

    /**
     * Container's getter for YmsuCustomersGrupVVO1.
     * @return YmsuCustomersGrupVVO1
     */
    public ViewObjectImpl getYMSUCustomersGrupVVO() {
        return (ViewObjectImpl)findViewObject("YMSUCustomersGrupVVO");
    }
    
    private static int VARCHAR2 = Types.VARCHAR;

    protected Object callStoredFunction(int sqlReturnType, String stmt,
                                        Object[] bindVars) {
        CallableStatement st = null;
        try {
            st = getDBTransaction().createCallableStatement("begin ? := " + stmt + ";end;", 0);
            st.registerOutParameter(1, sqlReturnType);
            if (bindVars != null) {
                for (int z = 0; z < bindVars.length; z++) {
                    st.setObject(z + 2, bindVars[z]);
                }
            }
            st.executeUpdate();
            return st.getObject(1);
        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                }
            }

        }
    }

    protected void callStoredProcedureMail(String stmt,
                                        Object[] bindVars) {
        CallableStatement st = null;
        try {
            st = getDBTransaction().createCallableStatement("begin " + stmt + ";end;", 0);
            if (bindVars != null) {
                for (int z = 0; z < bindVars.length; z++) {
                    st.setObject(z + 1, bindVars[z]);
                }
            }
            st.executeUpdate();
        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                }
            }

        }
    } 
    
    protected void setPassKey(String emailId, String key)       
    {
            callStoredProcedure("set_pass_key(?, ?)", new Object[] {emailId, key});
    }

    public String getUserName(String param) {
        return (String)callStoredFunction(VARCHAR2, "get_user_name(?)", new Object[] { param });
    }    
    
    public String addMemberToGroup(String groupName, String username){
        WlsUtils wls = new WlsUtils();
        wls.addMember(groupName, username);
        return "";
    }
    
    public String getUserGroups(String user){
        WlsUtils wls = new WlsUtils();
        List group = wls.getUserGroups(user);
        System.out.println(group.get(0));
        return "";
    }

    /**
     * Container's getter for YpouUsersVO2.
     * @return YpouUsersVO2
     */
    public ViewObjectImpl getYpouUsersForYoneticiVO() {
        return (ViewObjectImpl)findViewObject("YpouUsersForYoneticiVO");
    }

    /**
     * Container's getter for YmsuCustomersGrupVVO1.
     * @return YmsuCustomersGrupVVO1
     */
    public ViewObjectImpl getYmsuCustomersGrupVForParentVO() {
        return (ViewObjectImpl)findViewObject("YmsuCustomersGrupVForParentVO");
    }

    /**
     * Container's getter for YmsuTabletSatisTemsilcisiVO2.
     * @return YmsuTabletSatisTemsilcisiVO2
     */
    public ViewObjectImpl getYmsuSubTabletSatisTemsilcisiVO1() {
        return (ViewObjectImpl)findViewObject("YmsuSubTabletSatisTemsilcisiVO1");
    }
}
