package model;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import javax.naming.Context;

public class WlsUtils {
    private MBeanServerConnection connection;
    private JMXConnector connector;
    private static final String MBEAN_INTERFACE =
        "weblogic.security.providers.authentication.DefaultAuthenticatorMBean";
    public static final String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
    public static final String MBEAN_SERVER = "weblogic.management.mbeanservers.domainruntime";
    public static final String JNDI_ROOT = "/jndi/";
    public static final String DEFAULT_PROTOCOL = "t3";
    public static final String PROTOCOL_PROVIDER_PACKAGES = "weblogic.management.remote";
    //This how we get our DomainRuntimeService, this is where DomainConfigurationMBeans exists
    public static final String DOMAIN_MBEAN_NAME =
        "com.bea:Name=DomainRuntimeService,Type=weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean";
    private static ObjectName defaultAuthenticator;
    private static ObjectName[] authenticationProviders;
    private static String authenticatorName = "DefaultAuthenticator";


    public WlsUtils() {
        super();
        connect();
    }
    //no need any customization

    public void connect() {
        String hostname = "10.0.3.241";
        String username = "weblogic";
        String password = "yataslogic123";
        int port = 7001;
        connect(hostname, username, password, port);
    }

    //no need any customization

    public void connect(String hostname, String username, String password, int port) {
        try {
            String protocol = "t3";
            String jndi = "/jndi/weblogic.management.mbeanservers.domainruntime";
            JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndi);
            Hashtable env = new Hashtable();
            env.put(Context.SECURITY_PRINCIPAL, username);
            env.put(Context.SECURITY_CREDENTIALS, password);
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
            env.put("jmx.remote.x.request.waiting.timeout", new Long(10000));
            connector = JMXConnectorFactory.connect(serviceURL, env);
            connection = connector.getMBeanServerConnection();
            //providerON = getAuthenticationProviderObjectName(MBEAN_INTERFACE);

            ObjectName configurationMBeans = new ObjectName(DOMAIN_MBEAN_NAME);
            ObjectName domain = (ObjectName)connection.getAttribute(configurationMBeans, "DomainConfiguration");
            ObjectName security = (ObjectName)connection.getAttribute(domain, "SecurityConfiguration");

            ObjectName realm = (ObjectName)connection.getAttribute(security, "DefaultRealm");

            authenticationProviders = (ObjectName[])connection.getAttribute(realm, "AuthenticationProviders");

            for (int i = 0; i < authenticationProviders.length; i++) {
                String name = (String)connection.getAttribute(authenticationProviders[i], "Name");

                if (name.equals(authenticatorName))
                    defaultAuthenticator = authenticationProviders[i];
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    //no need any customization

    public void close() {
        try {
            connector.close();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public boolean resetUserPassword(String username, String newPassword) {
        try {
            if (!username.equalsIgnoreCase("weblogic")) {
                connection.invoke(defaultAuthenticator, "resetUserPassword", new Object[] { username, newPassword },
                                  new String[] { "java.lang.String", "java.lang.String" });

            }

            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //no need any customization

    public boolean isUserExists(String currentUser) throws RuntimeException {
        try {
            boolean userExists =
                ((Boolean)connection.invoke(defaultAuthenticator, "userExists", new Object[] { currentUser },
                                            new String[] { "java.lang.String" })).booleanValue();

            return userExists;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean isGroupExists(String currentGroup) throws RuntimeException {
        try {
            boolean groupExists =
                ((Boolean)connection.invoke(defaultAuthenticator, "groupExists", new Object[] { currentGroup },
                                            new String[] { "java.lang.String" })).booleanValue();

            return groupExists;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public String createUser(String userId, String userPassword, String userDescription, String eMail,
                             ArrayList groupNames) {
        if (userPassword == null)
            userPassword = PasswordUtils.generateInitialPassword();
        if (isUserExists(userId)) {
            return "Kullanıcı Adı Daha Önce Kullanılmıştır!";
        }
        try {
            connection.invoke(defaultAuthenticator, "createUser",
                              new Object[] { userId, userPassword, userDescription },
                              new String[] { "java.lang.String", "java.lang.String", "java.lang.String" });
            for (int i = 0; i < groupNames.size(); i++) {
                addMember(groupNames.get(i).toString(), userId);
            }
        } catch (Exception e) {
            return "fail";
        }

        return "success";
    }

    public String removeUser(String userName) {
        if (isUserExists(userName)) {
            try {
                connection.invoke(defaultAuthenticator, "removeUser", new Object[] { userName },
                                  new String[] { "java.lang.String" });
                return "success";
            } catch (Exception ex) {
                return "fail";
            }
        }
        return "belirtti?iniz isimde bir kullan?c? sistemde kay?tl? de?ildir.";
    }

    public String removeGroup(String groupName) {
        if (isGroupExists(groupName)) {
            try {
                connection.invoke(defaultAuthenticator, "removeGroup", new Object[] { groupName },
                                  new String[] { "java.lang.String" });
                return "success";
            } catch (Exception ex) {
                return "fail";
            }
        }
        return "belirtti?iniz grup ismi sistemde kay?tl? de?ildir.";
    }

    public List<String> getListOfUsers() throws RuntimeException {
        try {
            List<String> allUsers = new ArrayList<String>();

            String cursor =
                (String)connection.invoke(defaultAuthenticator, "listUsers", new Object[] { "*", Integer.valueOf(9999) },
                                          new String[] { "java.lang.String", "java.lang.Integer" });

            boolean haveCurrent =
                ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor },
                                            new String[] { "java.lang.String" })).booleanValue();

            while (haveCurrent) {
                String currentName =
                    (String)connection.invoke(defaultAuthenticator, "getCurrentName", new Object[] { cursor },
                                              new String[] { "java.lang.String" });


                allUsers.add(currentName);
                connection.invoke(defaultAuthenticator, "advance", new Object[] { cursor },
                                  new String[] { "java.lang.String" });

                haveCurrent =
                        ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor }, new String[] { "java.lang.String" })).booleanValue();
            }

            return allUsers;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<String> getUserGroups(String username) throws RuntimeException {
        try {
            List<String> allUserGroups = new ArrayList<String>();

            String cursor =
                (String)connection.invoke(defaultAuthenticator, "listMemberGroups", new Object[] { username },
                                          new String[] { "java.lang.String" });

            boolean haveCurrent =
                ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor },
                                            new String[] { "java.lang.String" })).booleanValue();

            while (haveCurrent) {
                String currentName =
                    (String)connection.invoke(defaultAuthenticator, "getCurrentName", new Object[] { cursor },
                                              new String[] { "java.lang.String" });

                allUserGroups.add(currentName);

                connection.invoke(defaultAuthenticator, "advance", new Object[] { cursor },
                                  new String[] { "java.lang.String" });

                haveCurrent =
                        ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor }, new String[] { "java.lang.String" })).booleanValue();
            }

            return allUserGroups;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<String> getGroupMembers(String groupName) throws RuntimeException {
        try {
            List<String> allGroupMembers = new ArrayList<String>();

            String cursor =
                (String)connection.invoke(defaultAuthenticator, "listGroupMembers", new Object[] { groupName, "*",
                                                                                                   new java.lang.Integer(0) },
                                          new String[] { "java.lang.String", "java.lang.String",
                                                         "java.lang.Integer" });

            boolean haveCurrent =
                ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor },
                                            new String[] { "java.lang.String" })).booleanValue();

            while (haveCurrent) {
                String currentName =
                    (String)connection.invoke(defaultAuthenticator, "getCurrentName", new Object[] { cursor },
                                              new String[] { "java.lang.String" });

                allGroupMembers.add(currentName);

                connection.invoke(defaultAuthenticator, "advance", new Object[] { cursor },
                                  new String[] { "java.lang.String" });

                haveCurrent =
                        ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor }, new String[] { "java.lang.String" })).booleanValue();
            }

            return allGroupMembers;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public List listGroups(String groupName) {
        try {
            List lst = new ArrayList();
            String cursor =
                (String)connection.invoke(defaultAuthenticator, "listGroups", new Object[] { groupName, 0 },
                                          new String[] { "java.lang.String", "java.lang.Integer" });

            boolean haveCurrent =
                ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor },
                                            new String[] { "java.lang.String" })).booleanValue();

            while (haveCurrent) {
                String currentName =
                    (String)connection.invoke(defaultAuthenticator, "getCurrentName", new Object[] { cursor },
                                              new String[] { "java.lang.String" });
                lst.add(currentName);

                connection.invoke(defaultAuthenticator, "advance", new Object[] { cursor },
                                  new String[] { "java.lang.String" });

                haveCurrent =
                        ((Boolean)connection.invoke(defaultAuthenticator, "haveCurrent", new Object[] { cursor }, new String[] { "java.lang.String" })).booleanValue();
            }

            return lst;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void addMember(String groupName, String username){
        try {
            connection.invoke(defaultAuthenticator, "addMemberToGroup", new Object[] { groupName, username },
                              new String[] { "java.lang.String", "java.lang.String" });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
