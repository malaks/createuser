package Utils;

import java.util.UUID;

public class GenerateUUID {
    public GenerateUUID() {
        super();
    }

    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}