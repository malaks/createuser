package Utils;
import java.util.Random;

public class PasswordUtil {
    private PasswordUtil() {
    }
    public static String generateInitialPassword()
    {        
        String password = "";
        int len = 8;
        String passwordNumber = "0123456789";
        String passwordText = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        password = "" + passwordNumber.charAt((int)(Math.random() * 10));
        for(int i = 0; i < len-1; i++)
        {
            password = password + passwordText.charAt((int)(Math.random() * 62));
        }
        return password;
    }
}
