package Utils;


import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import java.util.Set;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public final class JSFUtils {
    private static final String NO_RESOURCE_FOUND = "Missing resource: ";

    /**
     * Don't allow instantiation of this utility type class.
     * @writer Rudy De Busscher
     */
    private JSFUtils() {
    }

    /**
     * This method will handle a navigation case programmatically.
     * @param outcome
     */
    public static void handleNavigation(String outcome) {
        FacesContext ctx = JSFUtils.getFacesContext();
        ctx.getApplication().getNavigationHandler().handleNavigation(ctx, "", outcome);
    }

    /**
     * Method for taking a reference to a JSF binding expression and returning
     * the matching object (or creating it).
     * @param expression EL expression
     * @return Managed object
     */
    public static Object resolveExpression(String expression) {
        FacesContext facesContext = getFacesContext();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp.getValue(elContext);
    }

    public static void redirectPage(String pagename) {
       /* 
        FacesContext fc = FacesContext.getCurrentInstance();
        UIViewRoot iViewRoot = fc.getViewRoot();
        String viewId="/"+pagename;
        ViewHandler handler = fc.getApplication().getViewHandler();
        System.out.println("�nceki getViewId() : "+iViewRoot.getViewId());
        UIViewRoot viewRoot = handler.createView(fc, viewId);  
        System.out.println("Sonraki getViewId() : "+viewRoot.getViewId());
        fc.setViewRoot(viewRoot);       
        fc.renderResponse();         
        */
        
        String url = getRequestContextPath() + "" + getRequestServletPath() + "/" + pagename;
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        ExternalContext context = fctx.getExternalContext();
        try {
            ectx.redirect(url);
            fctx.responseComplete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String resolveRemoteUser() {
        FacesContext facesContext = getFacesContext();
        ExternalContext ectx = facesContext.getExternalContext();
        return ectx.getRemoteUser();
    }

    public static String resolveUserPrincipal() {
        FacesContext facesContext = getFacesContext();
        ExternalContext ectx = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest)ectx.getRequest();
        return request.getUserPrincipal().getName();
    }

    public static Object resolveMethodExpression(String expression, Class returnType, Class[] argTypes, Object[] argValues) {
        FacesContext facesContext = getFacesContext();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        MethodExpression methodExpression = elFactory.createMethodExpression(elContext, expression, returnType, argTypes);
        return methodExpression.invoke(elContext, argValues);
    }

    /**
     * Method for taking a reference to a JSF binding expression and returning
     * the matching Boolean.
     * @param expression EL expression
     * @param paTargetClass Class used to cast the result of the method.
     * @return Managed object
     */
    public static <T> T resolveExpression(String expression, Class<T> paTargetClass) {
        return (T)resolveExpression(expression);
    }

    /**
     * Convenience method for resolving a reference to a managed bean by name
     * rather than by expression.
     * @param beanName name of managed bean
     * @return Managed object
     */
    public static Object getManagedBeanValue(String beanName) {
        StringBuffer buff = new StringBuffer("#{");
        buff.append(beanName);
        buff.append('}');
        return resolveExpression(buff.toString());
    }

    /**
     * Method for setting a new object into a JSF managed bean
     * Note: will fail silently if the supplied object does
     * not match the type of the managed bean.
     * @param expression EL expression
     * @param newValue new value to set
     */
    public static void setExpressionValue(String expression, Object newValue) {
        FacesContext facesContext = getFacesContext();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        //Check that the input newValue can be cast to the property type
        //expected by the managed bean.
        //If the managed Bean expects a primitive we rely on Auto-Unboxing
        //I could do a more comprehensive check and conversion from the object
        //to the equivilent primitive but life is too short
        Class bindClass = valueExp.getType(elContext);
        if (bindClass.isPrimitive() || bindClass.isInstance(newValue)) {
            valueExp.setValue(elContext, newValue);
        }
    }

    /**
     * Convenience method for setting the value of a managed bean by name
     * rather than by expression.
     * @param beanName name of managed bean
     * @param newValue new value to set
     */
    public static void setManagedBeanValue(String beanName, Object newValue) {
        StringBuffer buff = new StringBuffer("#{");
        buff.append(beanName);
        buff.append('}');
        setExpressionValue(buff.toString(), newValue);
    }

    /**
     * Convenience method for setting Session variables.
     * @param key object key
     * @param object value to store
     */
    public static void storeOnSession(String key, Object object) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        sessionState.put(key, object);
    }

    /**
     * Convenience method for getting Session variables.
     * @param key object key
     * @return session object for key
     */
    public static Object getFromSession(String key) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        return sessionState.get(key);
    }

    public static String getFromHeader(String key) {
        FacesContext ctx = getFacesContext();
        ExternalContext ectx = ctx.getExternalContext();
        return ectx.getRequestHeaderMap().get(key);
    }

    /**
     * Convenience method for getting Request variables.
     * @param key object key
     * @return session object for key
     */
    public static Object getFromRequest(String key) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getRequestMap();
        return sessionState.get(key);
    }

    /**
     * Pulls a String resource from the property bundle that
     * is defined under the application &lt;message-bundle&gt; element in
     * the faces config. Respects Locale
     * @param key string message key
     * @return Resource value or placeholder error String
     */
    public static String getStringFromBundle(String key) {
        ResourceBundle bundle = getBundle();
        return getStringSafely(bundle, key, null);
    }

    /**
     * Convenience method to construct a <code>FacesMesssage</code>
     * from a defined error key and severity
     * This assumes that the error keys follow the convention of
     * using <b>_detail</b> for the detailed part of the
     * message, otherwise the main message is returned for the
     * detail as well.
     * @param key for the error message in the resource bundle
     * @param severity severity of message
     * @return Faces Message object
     */
    public static FacesMessage getMessageFromBundle(String key, FacesMessage.Severity severity) {
        ResourceBundle bundle = getBundle();
        String summary = getStringSafely(bundle, key, null);
        String detail = getStringSafely(bundle, key + "_detail", summary);
        FacesMessage message = new FacesMessage(summary, detail);
        message.setSeverity(severity);
        return message;
    }

    /**
     * Add JSF info message.
     * @param msg info message string
     */
    public static void addFacesInformationMessage(String msg) {
        FacesContext ctx = getFacesContext();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "");
        ctx.addMessage(null, fm);
    }

    // Informational getters

    /**
     * Get view id of the view root.
     * @return view id of the view root
     */
    public static String getRootViewId() {
        return getFacesContext().getViewRoot().getViewId();
    }

    /**
     * Get component id of the view root.
     * @return component id of the view root
     */
    public static String getRootViewComponentId() {
        return getFacesContext().getViewRoot().getId();
    }

    /**
     * Get FacesContext.
     * @return FacesContext
     */
    public static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Internal method to pull out the correct local
     * message bundle
     */
    public static ResourceBundle getBundle() {
        FacesContext ctx = getFacesContext();
        UIViewRoot uiRoot = ctx.getViewRoot();
        Locale locale = uiRoot.getLocale();
        ClassLoader ldr = Thread.currentThread().getContextClassLoader();
        return ResourceBundle.getBundle(ctx.getApplication().getMessageBundle(), locale, ldr);
    }

    /**
     * Get an HTTP Request attribute.
     * @param name attribute name
     * @return attribute value
     */
    public static Object getRequestAttribute(String name) {
        return getFacesContext().getExternalContext().getRequestMap().get(name);
    }

    /**
     * Set an HTTP Request attribute.
     * @param name attribute name
     * @param value attribute value
     */
    public static void setRequestAttribute(String name, Object value) {
        getFacesContext().getExternalContext().getRequestMap().put(name, value);
    }
    /*
   * Internal method to proxy for resource keys that don't exist
   */

    private static String getStringSafely(ResourceBundle bundle, String key, String defaultValue) {
        String resource = null;
        try {
            resource = bundle.getString(key);
        } catch (MissingResourceException mrex) {
            if (defaultValue != null) {
                resource = defaultValue;
            } else {
                resource = NO_RESOURCE_FOUND + key;
            }
        }
        return resource;
    }

    /**
     * Locate an UIComponent in view root with its component id. Use a recursive way to achieve this.
     * Taken from <a href="http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent">http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent</a>
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponentInRoot(String id) {
        UIComponent component = null;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            component = findComponent(root, id);
        }
        return component;
    }

    /**
     * Locates all components.
     * @return UIComponent object
     */
    public static List<UIComponent> findAllComponentsinRoot() {
        List<UIComponent> result = new ArrayList<UIComponent>();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            result = root.getChildren();
        }
        return result;
    }

    /**
     * Locate an UIComponent from its root component.
     * Taken from <a href="http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent">http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent</a>
     * @param base root Component (parent)
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId())) {
            return base;
        }
        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent)childrens.next();
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public static String getRequestContextPath() {
        ExternalContext context = getFacesContext().getExternalContext();
        return context.getRequestContextPath();
    }

    public static String getRequestServletPath() {
        ExternalContext context = getFacesContext().getExternalContext();
        return context.getRequestServletPath();
    }

    public static String getServletContextPath() {
        ExternalContext context = getFacesContext().getExternalContext();
        ServletContext servletContext = (ServletContext)context.getContext();
        return servletContext.getRealPath("/");
    }

    /**
     * Locate an UIComponent from a base component and looking in his parents.
     * @param base root Component
     * @param id UIComponent id to search
     * @return UIComponent object
     */
    public static UIComponent findClosestComponent(UIComponent base, String id) {
        if (id.equals(base.getId())) {
            return base;
        }
        UIComponent child = null;
        UIComponent result = null;
        Iterator children = base.getParent().getFacetsAndChildren();
        while (children.hasNext() && (result == null)) {
            child = (UIComponent)children.next();
            result = findComponent(child, id);
        }
        if (result == null && !(base instanceof UIViewRoot)) {
            result = findClosestComponent(base.getParent(), id);
        }
        return result;
    }

    /**
     * Determines all the components that have the value specified as parameter as their id.
     * @param id
     * @return
     * @writer Rudy De Busscher
     */
    public static List<UIComponent> findComponentsInRoot(String id) {
        List<UIComponent> result = new ArrayList<UIComponent>();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            findComponents(result, root, id);
        }
        return result;
    }

    /**
     * Locate all the UIComponents from its root component.
     * @param components The list that will be populated with the found components
     * @param base root Component (parent)
     * @param id UIComponent id
     * @writer Rudy De Busscher
     */
    public static void findComponents(List<UIComponent> components, UIComponent base, String id) {
        if (id.equals(base.getId())) {
            components.add(base);
            return;
        }
        UIComponent child = null;
        Iterator children = base.getFacetsAndChildren();
        while (children.hasNext()) {
            child = (UIComponent)children.next();
            if (id.equals(child.getId())) {
                components.add(child);
                break;
            }
            findComponents(components, child, id);
        }
    }

    /**
     * Method to create a redirect URL. The assumption is that the JSF servlet mapping is
     * "faces", which is the default
     *
     * @param view the JSP or JSPX page to redirect to
     * @return a URL to redirect to
     */
    public static String getPageURL(String view) {
        FacesContext facesContext = getFacesContext();
        ExternalContext externalContext = facesContext.getExternalContext();
        String url = ((HttpServletRequest)externalContext.getRequest()).getRequestURL().toString();
        StringBuffer newUrlBuffer = new StringBuffer();
        newUrlBuffer.append(url.substring(0, url.lastIndexOf("faces/")));
        newUrlBuffer.append("faces");
        String targetPageUrl = view.startsWith("/") ? view : "/" + view;
        newUrlBuffer.append(targetPageUrl);
        return newUrlBuffer.toString();
    }

    /**
     * Refresh the current page.
     */
    public static void refreshCurrentPage() {
        FacesContext context = FacesContext.getCurrentInstance();
        String currentView = context.getViewRoot().getViewId();
        ViewHandler vh = context.getApplication().getViewHandler();
        UIViewRoot x = vh.createView(context, currentView);
        context.setViewRoot(x);
    }

    /**
     * Force the validation and updates model phase.  Usefull in the situation where
     * we have an ilmmediate true action but want to keep the values.
     * @param paComponent The component to validate, call it with the view root.
     * @param paContext The FacesContext
     * @writer Rudy De Busscher
     */
    public static void forceValidateAndUpdateField(UIComponent paComponent, FacesContext paContext) {
        if (paComponent instanceof EditableValueHolder) {
            EditableValueHolder field = (EditableValueHolder)paComponent;
            paComponent.processValidators(paContext);
            field.setValid(true); // In case something went wrong, just ignore it now.
            paComponent.processUpdates(paContext);
        }
        Iterator<UIComponent> iter = paComponent.getFacetsAndChildren();
        while (iter.hasNext()) {
            UIComponent childOrFacet = iter.next();
            forceValidateAndUpdateField(childOrFacet, paContext);
        }
    }

    /**
     * Helper method to execute the an Method EL with one parameter.
     * @param expr the EL expression
     * @param paParameter
     */
    public static void invokeMethodExpression(String expr, Object paParameter) {
        invokeMethodExpression(expr, Object.class, new Class[] { paParameter.getClass() }, new Object[] { paParameter });
    }

    /**
     * Helper method to execute the QueryListener EL for methods with a return type.
     * @param expr The method you want to execute
     * @param returnType The return type of the method
     * @param argTypes The types of the arguments
     * @param args the arguments passed to the method (must match argTypes)
     */
    public static Object invokeMethodExpression(String expr, Class returnType, Class[] argTypes, Object[] args) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        MethodExpression methodExpr = elFactory.createMethodExpression(elctx, expr, returnType, argTypes);
        return methodExpr.invoke(elctx, args);
    }

    public static Object invokeMethodExpression(String expr, Class returnType, Class argType, Object argument) {
        return invokeMethodExpression(expr, returnType, new Class[] { argType }, new Object[] { argument });
    }

    public static String resolveFromResourceBundle(String paResourceBundle, String paKey) {
        StringBuilder expression = new StringBuilder();
        expression.append("#{adfBundle['").append(paResourceBundle).append("'].").append(paKey).append("}");
        return resolveExpression(expression.toString(), String.class);
    }
    private static final String DATEFORMATPATTERN = "dd/MM/yyyy";

    /**
     * This method will fetch a string from the resourcebundle for a specific key.
     * Afterwards it will replace all the keys from the dynamicParams map with
     * their corresponding values.
     * When the value is of type java.sql.Date, the Date will be transformed into
     * a String using the format 'DD/MM/YYYY'.
     *
     * @param paResourceBundle Path + Name of the resourceBundle
     * @param paKey The key in the resourceBundle to fetch the text
     * @param dynamicParams A map of dynamic constants to be replaced in the string of the resourcebundle
     * @return
     * @writer Filip Huysmans
     */
    public static String resolveFromResourceBundle(String paResourceBundle, String paKey, Map dynamicParams) {
        // Determine the string from the resource bundle
        String resourceBundleValue = resolveFromResourceBundle(paResourceBundle, paKey);
        // Replace the dynamic parts of the string by looping through the map
        if (dynamicParams != null && !dynamicParams.isEmpty()) {
            for (Map.Entry entry : (Set<Map.Entry>)dynamicParams.entrySet()) {
                Object value = entry.getValue();
                // Transforming the key to the exact representation in the resource bundle string
                String key = "\\{" + entry.getKey() + "\\}";
                // Handling some special cases of a date
                if (value instanceof java.sql.Date) {
                    // Format the date before putting it into the string
                    java.sql.Date newDate = (java.sql.Date)value;
                    SimpleDateFormat formatDate = new SimpleDateFormat(DATEFORMATPATTERN);
                    value = formatDate.format(newDate);
                }
                // Replacing all occurances of the key with the value
                resourceBundleValue = resourceBundleValue.replaceAll(key, (String)value);
            }
        }
        // Returning the result String
        return resourceBundleValue;
    }

    public static void writeJavaScriptToClient(String paScript) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = null;
        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        erks.addScript(fctx, paScript);
    }

    /**
     * Get the value of an EL expression.
     * @param expr
     * @return
     */
    public static Object get(String expr) {
        FacesContext fc = JSFUtils.getFacesContext();
        return fc.getApplication().evaluateExpressionGet(fc, expr, Object.class);
    }

    /**
     * Set the a value into an EL Expression
     * @param expr
     * @param value
     */
    public static void set(String expr, String value) {
        Object valToSet = value;
        if (isELExpr(value)) {
            valToSet = get(value);
        }
        set(expr, valToSet);
    }

    /**
     * Check whether the given parameter is a valid EL Expression
     * @param o
     * @return
     */
    private static boolean isELExpr(Object o) {
        if (o instanceof String) {
            String str = (String)o;
            str = str.trim();
            return str.startsWith("#{") && str.endsWith("}");
        }
        return false;
    }

    /**
     * Set the a value into an EL Expression
     * @param expr
     * @param value
     */
    public static void set(String expr, Object value) {
        FacesContext fc = JSFUtils.getFacesContext();
        ELContext elc = fc.getELContext();
        ExpressionFactory ef = fc.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(elc, expr, Object.class);
        ve.setValue(elc, value);
    }

    public static void addFacesMessageError(String componentId, String message) {
        FacesContext context = getFacesContext();
        context.addMessage(componentId, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message));
    }

    public static void addFacesMessage(String id, FacesMessage fm) {
        JSFUtils.getFacesContext().addMessage(id, fm);
    }

    /**
     * Add JSF error message.
     * @param msg error message string
     */
    public static void addFacesErrorMessage(String msg) {
        FacesContext ctx = getFacesContext();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "");
        ctx.addMessage(null, fm);
    }

    /**
     * Add JSF warning message.
     * @param msg warning message string
     */
    public static void addFacesWarningMessage(String msg) {
        FacesContext ctx = getFacesContext();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, msg, "");
        ctx.addMessage(null, fm);
    }
    public static void addFacesErrorMessageForComponet(UIComponent component,String message){
        AdfFacesContext adfFacesContext = null;
        adfFacesContext = AdfFacesContext.getCurrentInstance();
        FacesContext ctx = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, "");
        ctx.addMessage(component.getClientId(ctx), fm);
    }

    /**
     * This method will set the focus on a field in the current row from a table
     * @param table The reference to the table component
     * @param fieldId The name(=Id) of the field to focus on
     * @writer Filip Huysmans
     */
    public static void setFocusOnField(RichTable table, String fieldId) {
        // Getting the iterator from the table component
        CollectionModel cm = (CollectionModel)table.getValue();
        JUCtrlHierBinding tb = (JUCtrlHierBinding)cm.getWrappedData();
        DCIteratorBinding icd = tb.getDCIteratorBinding();
        // Take the key of the current row and set the current row active
        ArrayList lst = new ArrayList(1);
        lst.add(icd.getCurrentRow().getKey());
        table.setActiveRowKey(lst);
        // Looking for the necessary id's to create the focus call
        String tableId = table.getClientId(JSFUtils.getFacesContext());
        RowKeySet rks = table.getSelectedRowKeys();
        if (rks != null && rks.size() > 0) {
            // Only when there are rows in the table
            Object rowKey = rks.iterator().next();
            String rowId = table.getClientRowKeyManager().getClientRowKey(JSFUtils.getFacesContext(), table, rowKey);
            String inputId = tableId + ":" + rowId + ":" + fieldId;
            // Creating the Javascript to put the focus on the field
            ExtendedRenderKitService service = Service.getRenderKitService(JSFUtils.getFacesContext(), ExtendedRenderKitService.class);
            // Adding a timeout to wait for the components to render
            service.addScript(JSFUtils.getFacesContext(),
                              "setTimeout(function(){comp = AdfPage.PAGE.findComponent('" + inputId + "'); comp.focus();}, 500)");
        }
    }

    /**
     * method for removing Session variables.
     * @param
     * @writer Christophe Gobled
     */
    public static void removeFromSession(String key) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        sessionState.remove(key);
    }
}
