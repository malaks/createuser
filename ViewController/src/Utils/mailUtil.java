package Utils;

import com.sun.mail.smtp.SMTPTransport;

import java.io.Serializable;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;

public class mailUtil{

    public mailUtil() {
        super();
    }

    String emailID = null;
    String emailSubject = null;
    String emailText = null;

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public String getEmailText() {
        return emailText;
    }

    public void sendMail() throws Exception {
        InitialContext ic = new InitialContext();
        Session session = (Session)ic.lookup("mail/YatasMailSession");

        Properties props = session.getProperties();

        String to = emailID;

        String mailhost = props.getProperty("mail.smtp.host");
        String user = props.getProperty("mail.smtp.user");
        String password = props.getProperty("mail.smtp.password");
        String protocol = props.getProperty("mail.transport.protocol");
        String mailDisabled = props.getProperty("mail.disable");
        String verboseProp = props.getProperty("mail.verbose");

        boolean sentDisabled = false;
        if (mailDisabled != null && mailDisabled.equals("true"))
            sentDisabled = true;

        if (!sentDisabled) {

            boolean verbose = false;
            if (verboseProp != null && verboseProp.equals("true"))
                verbose = true;

            String mailer = "smtpsend";
            Message msg = new MimeMessage(session);
            msg.setFrom();
            msg.setRecipients(Message.RecipientType.TO,
                              InternetAddress.parse(to, false));

            msg.setSubject(emailSubject);
            msg.setText(emailText);

            msg.setHeader("X-Mailer", mailer);
            msg.setHeader("MIME-Version", "1.0");
            msg.setHeader("Content-Type", "text/html");
            msg.setHeader("Content-Transfer-Encoding", "quoted-printable"); 
            msg.setHeader("charset", "UTF-8");

            msg.setSentDate(new Date()); 

            SMTPTransport t = (SMTPTransport)session.getTransport(protocol);

            try {
                t.connect(mailhost, user, password);
                t.sendMessage(msg, msg.getAllRecipients());
            } finally {
                if (verbose)
                    t.close();
            }

            //Mail was sent successfully.
        }
    }

}
