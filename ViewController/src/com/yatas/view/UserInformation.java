package com.yatas.view;

import Utils.ADFUtils;

import javax.faces.event.ActionEvent;

import model.VO.YpouUsersVOImpl;
import model.VO.YpouUsersVORowImpl;

public class UserInformation {
    public UserInformation() {
    }
    private String userName, customerId;
    private boolean yoneticiMi;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setYoneticiMi(boolean yoneticiMi) {
        this.yoneticiMi = yoneticiMi;
    }

    public boolean isYoneticiMi() {
        return yoneticiMi;
    }

    public void currentUserClick(ActionEvent actionEvent) {
        YpouUsersVOImpl vo =(YpouUsersVOImpl)ADFUtils.findIterator("YpouUsersForYoneticiVOIterator").getViewObject();
        System.out.println(vo.getEstimatedRowCount());
        //YpouUsersVORowImpl currentRow = vo.getUser("weblogic");
        //System.out.println(currentRow.getCustomerId());
    }
}
