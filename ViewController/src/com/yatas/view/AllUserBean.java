package com.yatas.view;

import Utils.ADFUtils;

import java.util.List;

import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


import model.VO.YpouUsersVOImpl;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class AllUserBean {
    public AllUserBean() {
        /*YpouUsersVOImpl vo = (YpouUsersVOImpl)ADFUtils.findIterator("YpouUsersForYoneticiVOIterator").getViewObject();
        /*vo.setApplyViewCriteriaName("YpouUsersByUserName");
        vo.setNamedWhereClauseParam("userName", "weblogic");
        vo.executeQuery();
        System.out.println(vo.first());*/
        /*vo.getUser("weblogic");
        System.out.println(vo.first());*/
    }

    public void getAllUsers(ActionEvent actionEvent) {
        OperationBinding operation = ADFUtils.getBindingContainer().getOperationBinding("getAllUsers");
        List<String> deneme = (List<String>)operation.execute();
        
        for (int i = 0; i < deneme.size(); i++) {
            System.out.println(deneme.get(i));
        }
        System.out.println(deneme.size());
    }

    public void removeUser(ActionEvent actionEvent) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = facesContext.getViewRoot();
        RichInputText rt = (RichInputText)viewRoot.findComponent("it1");
        String userId = rt.getValue().toString();
        OperationBinding operation = ADFUtils.getBindingContainer().getOperationBinding("removeSelectedUser");
        Map operationParamsMap = operation.getParamsMap();
        operationParamsMap.put("username", userId);
        String result = (String)operation.execute();
        if(result.equals("success")){
            DCIteratorBinding binding = ADFUtils.findIterator("YpouUsersVO1Iterator");
            ViewObject vo = binding.getViewObject();
            Row[] row = vo.getFilteredRows("UName", userId);
            if(row.length > 0){
                row[0].remove();
                ADFUtils.getBindingContainer().getOperationBinding("Commit").execute();
            }
            return;
        }
        System.out.println("Silme i?lemi s?ras?nda bir hata olu?tu");
    }

    public void activeUser(ActionEvent actionEvent) {
        YpouUsersVOImpl vo = (YpouUsersVOImpl)ADFUtils.findIterator("YpouUsersForYoneticiVOIterator").getViewObject();
        /*vo.setApplyViewCriteriaName("YpouUsersByUserName");
        vo.setNamedWhereClauseParam("userName", "weblogic");
        vo.executeQuery();
        System.out.println(vo.first());*/
        vo.getUser("weblogic");
        System.out.println(vo.first());
    }
}
