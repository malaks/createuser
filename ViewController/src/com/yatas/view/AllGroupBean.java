package com.yatas.view;

import Utils.ADFUtils;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import javax.faces.model.SelectItem;

import model.createUserAppModuleImpl;

import oracle.adf.view.rich.component.rich.input.RichSelectItem;

import oracle.binding.OperationBinding;

public class AllGroupBean {
    public AllGroupBean() {
        createUserAppModuleImpl moduleImpl = (createUserAppModuleImpl)ADFUtils.getApplicationModuleForDataControl("createUserAppModuleDataControl");
        allGroups = moduleImpl.getGroups("*");
    }
    
    private String user;
    private String group;
    private List allGroups;

    public void getAllGroups(ActionEvent actionEvent) {
        OperationBinding operation = ADFUtils.getBindingContainer().getOperationBinding("getGroups");
        Map groupOperation = operation.getParamsMap();
        groupOperation.put("groupName", "*");
        allGroups = (List)operation.execute();
        for (int i =  0; i< allGroups.size(); i++)
        {
            System.out.println(allGroups.get(i));
        }
    }

    public void setAllGroups(List<String> allGroups) {
        this.allGroups = allGroups;
    }

    public List getAllGroups() {
        
        return allGroups;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void grubaEkle(ActionEvent actionEvent) {
        OperationBinding operation = ADFUtils.getBindingContainer().getOperationBinding("addMemberToGroup");
        Map params = operation.getParamsMap();
        params.put("groupName", group);
        params.put("username", user);
        operation.execute();
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }
}
