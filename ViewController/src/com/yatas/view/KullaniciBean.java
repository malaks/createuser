package com.yatas.view;


import Utils.ADFUtils;
import Utils.PasswordUtil;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import Utils.GenerateUUID;

import model.createUserAppModuleImpl;

import Utils.mailUtil;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class KullaniciBean {
    private String kullaniciAdi, customerName, customerId, orgId, personId, sehir, salesrepId, aciklama, eMail, ad, soyad, password, ustKullaniciAdi, kayitSonucu;
    private ArrayList kullaniciGrup = new ArrayList();
    private List<SelectItem> kullaniciGruplari = new ArrayList<SelectItem>();
    private RichInputComboboxListOfValues sehirCombo;
    private RichInputComboboxListOfValues customerCombo;
    private RichInputComboboxListOfValues kullaniciCombo;
    private RichSelectOneRadio magazaMi;
    private RichSelectOneRadio yoneticiMi;

    public KullaniciBean() {
        super();
        //fillKullaniciGruplari();
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    
    public String getUserInfo()
    {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserPrincipal().getName(); 
        //String _user = secCntx.getUserName();  
        return user.toUpperCase();
    }

    public String createUser_action() {
        DCIteratorBinding YPOUUsers = ADFUtils.findIterator("YpouUsersVO1Iterator");
        Row[] sameCustomers = YPOUUsers.getViewObject().getFilteredRows("CustomerId", customerId);
        String parentUser = "";
        for(int i = 0; i<sameCustomers.length; i++){
            if("1".equals(sameCustomers[i].getAttribute("TabletMagazaYoneticisiMi"))){
                parentUser = sameCustomers[i].getAttribute("TabletMagazaYoneticisiMi").toString();
                break;
            }
        }
        password = PasswordUtil.generateInitialPassword();
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("createUser");
        Map operationParamsMap = operationBinding.getParamsMap();
        operationParamsMap.put("userId", this.kullaniciAdi);
        operationParamsMap.put("userPassword", password);
        operationParamsMap.put("userDescription", this.aciklama);
        operationParamsMap.put("eMail", this.eMail);
        operationParamsMap.put("firstName", ad);
        operationParamsMap.put("lastName", soyad);
        operationParamsMap.put("parentUserId", getUserInfo());
        operationParamsMap.put("groupNames", this.kullaniciGrup);
        String result = (String)operationBinding.execute();
        if(operationBinding.getErrors().isEmpty()){
            if(result == "success"){
                createUserAppModuleImpl appModule = (createUserAppModuleImpl)ADFUtils.getApplicationModuleForDataControl("createUserAppModuleDataControl");
                //appModule.sendMail(emailId, mail)
                
                OperationBinding createInsert = ADFUtils.getBindingContainer().getOperationBinding("CreateInsert");
                createInsert.execute();
                Row currentRow = YPOUUsers.getCurrentRow();
                currentRow.setAttribute("CustomerId", customerId);
                currentRow.setAttribute("EMail", this.eMail);
                currentRow.setAttribute("OracleSalesrepId", salesrepId);
                currentRow.setAttribute("OrgId", orgId);
                currentRow.setAttribute("PersonId", personId);
                currentRow.setAttribute("Sehir", sehir);
                currentRow.setAttribute("TabletMagazaYoneticisiMi", customerId);
                currentRow.setAttribute("UDescription", this.aciklama);
                currentRow.setAttribute("UName", kullaniciAdi);
                currentRow.setAttribute("UFirstName", ad);
                currentRow.setAttribute("ULastName", soyad);
                currentRow.setAttribute("MagazaBayi", magazaMi.getValue());
                currentRow.setAttribute("TabletMagazaYoneticisiMi", yoneticiMi.getValue());
                currentRow.setAttribute("UPassword", "NEWUSERPassword");
                currentRow.setAttribute("UParentUser", parentUser);
                OperationBinding op = ADFUtils.getBindingContainer().getOperationBinding("Commit");
                    if(!op.getErrors().isEmpty())
                        System.out.println(op.getErrors());
                    op.execute();
                sendInitialMail(eMail, password);
                    return "success";
            }
        }
        /*DCIteratorBinding binding = ADFUtils.findIterator("YmsuIllerVO1Iterator");
        Row row = binding.getViewObject().getFilteredRows("SehirAdi", sehirCombo.getValue())[0];
        System.out.println(row.getAttribute("SehirAdi"));*/
        return "";
        /*if (operationBinding.getErrors().isEmpty()) {
            if (result == "success") {
                kayitSonucu = "Kullanıcı Oluşturuldu!";
                return "success";
            } else {
                kayitSonucu = result;
                return "fail";
            }
        } else {
            kayitSonucu = operationBinding.getErrors().get(0).toString();
            return "fail";
        }*/
    }
    public void fillKullaniciGruplari()
    {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("getGroups");
        Map operationParamsMap = operationBinding.getParamsMap();
        operationParamsMap.put("groupName", "*");
        List result = (List)operationBinding.execute();
        for (int i =  0; i< result.size(); i++)
        {
            this.kullaniciGruplari.add(new SelectItem(result.get(i).toString(), result.get(i).toString()));
        }
    }
        
    public void kullaniciGrupValueChangeListener(ValueChangeEvent valueChangeEvent)
    {
        ArrayList<Object> selectedValue = (ArrayList<Object>)valueChangeEvent.getNewValue();
        kullaniciGrup.clear();
        kullaniciGrup.addAll(selectedValue);
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getAd() {
        return ad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getEMail() {
        return eMail;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi.toUpperCase();
    }

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getAciklama() {
        return aciklama;
    }

    public synchronized List<SelectItem> getKullaniciGruplari() {
        return kullaniciGruplari;
    }

    public String getKayitSonucu() {
        return kayitSonucu;
    }

    public String yeniKullanici_Action() {
        ad = "";
        soyad ="";
        eMail ="";
        kullaniciAdi ="";
        aciklama = "";
        kayitSonucu = "";
        kullaniciGrup.clear();
        return "newUser";
    }

    public void setUstKullaniciAdi(String ustKullaniciAdi) {
        this.ustKullaniciAdi = ustKullaniciAdi;
    }

    public String getUstKullaniciAdi() {
        return ustKullaniciAdi;
    }

    public void setSehirCombo(RichInputComboboxListOfValues sehirCombo) {
        this.sehirCombo = sehirCombo;
    }

    public RichInputComboboxListOfValues getSehirCombo() {
        return sehirCombo;
    }

    public void setCustomerCombo(RichInputComboboxListOfValues customerCombo) {
        this.customerCombo = customerCombo;
    }

    public RichInputComboboxListOfValues getCustomerCombo() {
        return customerCombo;
    }

    public void setKullaniciCombo(RichInputComboboxListOfValues kullaniciCombo) {
        this.kullaniciCombo = kullaniciCombo;
    }

    public RichInputComboboxListOfValues getKullaniciCombo() {
        return kullaniciCombo;
    }

    public void setMagazaMi(RichSelectOneRadio magazaMi) {
        this.magazaMi = magazaMi;
    }

    public RichSelectOneRadio getMagazaMi() {
        return magazaMi;
    }

    public void setYoneticiMi(RichSelectOneRadio yoneticiMi) {
        this.yoneticiMi = yoneticiMi;
    }

    public RichSelectOneRadio getYoneticiMi() {
        return yoneticiMi;
    }

    public void customerChange(ValueChangeEvent valueChangeEvent) {
        //System.out.println(valueChangeEvent.getNewValue());
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = ADFUtils.findIterator("YMSUCustomersGrupVVOIterator").getCurrentRow();
        System.out.println(row.getAttribute("CustomerName"));
        System.out.println(row.getAttribute("CustomerId"));
        System.out.println(ADFUtils.findControlBinding("CustomerName").getInputValue());
        customerName = row.getAttribute("CustomerName").toString();
        customerId = row.getAttribute("CustomerId").toString();
        System.out.println(customerName);
        System.out.println(customerId);
    }

    public void temsilciChange(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = ADFUtils.findIterator("YmsuTabletSatisTemsilcisiVO1Iterator").getCurrentRow();
        System.out.println(row.getAttribute("Name"));
        System.out.println(row.getAttribute("PersonId"));
        System.out.println(row.getAttribute("SalesrepId"));
        String[] temp = row.getAttribute("Name").toString().split(", ");
        ad = temp[1];
        soyad = temp[0];
        personId = row.getAttribute("PersonId").toString();
        salesrepId = row.getAttribute("SalesrepId").toString();
        ViewObject vo = ADFUtils.findIterator("YmsuTabletSatisTemsilcisiVO1Iterator").getViewObject();
        vo.getFilteredRows("PersonId", personId);
        //vo.getFilteredRows(, arg1)
        System.out.println(ad);
        System.out.println(soyad);
        System.out.println(personId);
        System.out.println(salesrepId);
    }
    
    public void subCustomerChange(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = ADFUtils.findIterator("YmsuCustomersGrupVForParentVOIterator").getCurrentRow();
        System.out.println(row.getAttribute("Name"));
        System.out.println(row.getAttribute("PersonId"));
        System.out.println(row.getAttribute("SalesrepId"));
        String[] temp = row.getAttribute("Name").toString().split(", ");
        ad = temp[1];
        soyad = temp[0];
        personId = row.getAttribute("PersonId").toString();
        salesrepId = row.getAttribute("SalesrepId").toString();
        System.out.println(ad);
        System.out.println(soyad);
        System.out.println(personId);
        System.out.println(salesrepId);
    }
    
    private void sendInitialMail(String mail, String password){
        String to = mail;

        // Sender's email ID needs to be mentioned
        String from = "noreplay@yatas.com.tr";

        // Assuming you are sending email from localhost
        String host = "10.0.3.19";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        try{
           // Create a default MimeMessage object.
           MimeMessage message = new MimeMessage(session);

           // Set From: header field of the header.
           message.setFrom(new InternetAddress(from));

           // Set To: header field of the header.
           message.addRecipient(Message.RecipientType.TO,
                                    new InternetAddress(to));

           // Set Subject: header field
           message.setSubject("Yata? Kullan?c? Parolas?");

           // Now set the actual message
           message.setText(password);

           // Send message
           Transport.send(message);
           System.out.println("Sent message successfully....");
        }catch (MessagingException mex) {
           mex.printStackTrace();
        }
    }
    
    public void sehirChange(ValueChangeEvent valueChangeEvent) {
        sehir = valueChangeEvent.getNewValue().toString();
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setSehir(String sehir) {
        this.sehir = sehir;
    }

    public String getSehir() {
        return sehir;
    }

    public void setSalesrepId(String salesrepId) {
        this.salesrepId = salesrepId;
    }

    public String getSalesrepId() {
        return salesrepId;
    }

    public void kullaniciDuzenle(ActionEvent actionEvent) {
        Row[] row = ADFUtils.findIterator("YpouUsersVO1Iterator").getViewObject().getFilteredRows("UName", kullaniciAdi);
        if(row.length>0){
            System.out.println(row[0].getAttribute("EMail"));
            row[0].setAttribute("Sehir", sehir);
            row[0].setAttribute("TabletMagazaYoneticisiMi", magazaMi.getValue());
            row[0].setAttribute("PersonId", personId);
            row[0].setAttribute("OrgId", orgId);
            row[0].setAttribute("OracleSalesrepId", salesrepId);
            row[0].setAttribute("MagazaBayi",magazaMi.getValue());
            row[0].setAttribute("TabletMagazaYoneticisiMi",yoneticiMi.getValue());
            row[0].setAttribute("CustomerId", customerId);
            row[0].setAttribute("EMail", eMail);
            row[0].setAttribute("UParentUser", ustKullaniciAdi);
        }
        OperationBinding operation = ADFUtils.findOperation("Commit");
        if(operation.getErrors().isEmpty()){
            operation.execute();
        }
        else
            System.out.println("bir hata olu?tu.");
    }
}
